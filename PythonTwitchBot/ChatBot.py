# Documentation for twitch-python https://pypi.org/project/twitch-python/
# Documentation for pyodbc: https://github.com/mkleehammer/pyodbc/wiki
# Requires Python 3.7 or greater and Microsoft SQL Server 2017

import twitch
import pyodbc

conn = pyodbc.connect('Driver={SQL Server};Server=JAKE-PC\\SQLEXPRESS;Database=SmashGamble;Trusted_Connection=yes;')

isBetting = False
numOfCandidates = 0


def handle_message(message: twitch.chat.Message) -> None:
    cursor = conn.cursor()
    user: twitch.helix.User = message.user().display_name
    global isBetting
    global numOfCandidates

    if message.text.startswith('!balance'):
        userBalance = cursor.execute("select [balance] from [TwitchUser] where ([username] = '{}')".format(user)).fetchval()
        if(userBalance is None):
            message.chat.send(f'@{user} you have not yet registered for SmashGamble. Please type !join to register.')
            return
        message.chat.send(f'@{user} has a balance of {userBalance} units')
        return

    if(message.text.startswith('!join')):
        count = cursor.execute("select count([username]) from [TwitchUser] where ([username] = '{}')".format(user)).fetchval()
        if(count == 1):
            message.chat.send(f'@{user}, you have already registered!')
            return
        cursor.execute("insert into [TwitchUser] (username, balance) values ('{}', {})".format(user, 1000))
        conn.commit()
        message.chat.send(f'Congratulations @{user}, you are now a part of SmashGamble! You have been gifted 1000 credits!')
        print("Registered {}".format(user))
        return

    if(message.text.startswith('!start') and (user == 'SmashGamble' or user == 'SmashBookie') and not isBetting):
        parameters = message.text.split()
        if(len(parameters) == 1):
            message.chat.send(f'PLACE YOUR BETS!')
            numOfCandidates = 2
            isBetting = True
            return
        if(len(parameters) == 3):
            numOfMessages = int(parameters[1])
            numOfCandidates = int(parameters[2])
            for x in range(numOfMessages):
                message.chat.send(f'PLACE YOUR BETS!')
            isBetting = True
            return
        message.chat.send(f'@{user} COMMAND ERROR: !start <numOfMessages>')
        return

    if(message.text.startswith('!stop') and (user == 'SmashGamble' or user == 'SmashBookie') and isBetting):
        isBetting = False
        numOfCandidates = 0
        message.chat.send(f'BETS ARE IN!')
        return

    if(message.text.startswith('!bet')):
        if(not isBetting):
            message.chat.send(f'@{user} COMMAND ERROR: Betting has not begun yet!')
            return
        else:
            parameters = message.text.split()
            if(len(parameters) == 3):
                if(int(parameters[1]) > numOfCandidates or int(parameters[1]) < 1):
                    message.chat.send(f'There are only {numOfCandidates} candidates! @{user}')
                    return
                currBalance = cursor.execute("select [balance] from [TwitchUser] where ([username] = '{}')".format(user)).fetchval()
                if(currBalance - int(parameters[2]) < 0):
                    message.chat.send(f'@{user} your current balance is only {currBalance}!')
                    return
                params = (parameters[1], "{}".format(user), parameters[2])
                cursor.execute("{CALL placeBet (?,?,?)}", params)
                conn.commit()
                message.chat.send(f'@{user} bet {parameters[2]} on candidate {parameters[1]}')
                return
            else:
                message.chat.send(f'@{user} COMMAND ERROR: !bet <candidateOfBet> <amountOfCurrency>')
                return

    if(message.text.startswith('!help')):
        parameters = message.text.split()
        if(len(parameters) == 1 or len(parameters) > 2):
            message.chat.send(f'@{user} Available commands: join, balance, bet')
            message.chat.send('Type !help <command name> for more info on a specific command.')
            return
        if(len(parameters) == 2):
            if(parameters[1] == 'bet'):
                message.chat.send(f'@{user} type !bet <candidate> <amount> to place a bet when bets are open.')
                return
            if(parameters[1] == 'join'):
                message.chat.send(f'@{user} type !join to register with SmashGamble.')
                return
            if(parameters[1] == 'balance'):
                message.chat.send(f'@{user} type !balance to view your balance.')
                return
        return

    if(message.text.startswith('!clearBets') and (user == 'SmashGamble' or user == 'SmashBookie')):
        cursor.execute("{CALL clearBets}")
        conn.commit()
        message.chat.send(f'ALL BETS HAVE BEEN CLEARED!')
        return

    if(message.text.startswith('!payout') and (user == 'SmashGamble' or user == 'SmashBookie')):
        parameters = message.text.split()
        if(len(parameters) == 1):
            return
        cursor.execute("{CALL payout (?)}", parameters[1])
        conn.commit()
        cursor.execute("{CALL clearBets}")
        conn.commit()
        message.chat.send(f'Grats! Payouts on {parameters[1]} have been awarded')
        return


def main():
    chat = twitch.Chat(channel='#smashgamble',
                       nickname='smashbookie',
                       oauth='oauth:foq2ud0eklkoho881wfxfcimdiumbi',
                       helix=twitch.Helix(client_id='350xnhr1fwkmjdnpga9k75vfq7m5z3', use_cache=True))

    chat.subscribe(handle_message)


if __name__ == '__main__':
    main()
